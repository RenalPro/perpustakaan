from models import books as db

def add_book():
    data = {
        '_id' : '5',
        'nama' : 'rujak',
        'pengarang' : 'Udin',
        'tahun_terbit' : '2050',
        'genre' : 'ecchi'
    }

    db(**data).save()

def search():
    for doc in db.objects(pengarang='James Comey'):
        print(doc._id, doc.nama, doc.pengarang, doc.tahun_terbit, doc.genre)

def show_book():
    for doc in db.objects:
        print(doc._id, doc.nama, doc.pengarang, doc.tahun_terbit, doc.genre)

def search_id():
    result = db.objects(_id="606ea838b3275fa4adf16125")
    print(result.to_json())


def hapus():
    delku = db.objects(pengarang="Anthony Doerr")
    delku.delete()

def new():
    date = db.objects(pengarang='JJ Smith')
    date.update(pengarang='Raenald Syaputra')

if __name__=='__main__':
    add_book()
    search()
    show_book()
    search_id()
    hapus()
    new()
    exit()