from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument

Connection = connect(db = 'perpustakaan', host='localhost', port = 27017)

if Connection:
    print('MongoDB engine is connection')

class books(Document):
    _id = StringField(required=True, max_length=70)
    nama = StringField(required=True, max_length=70)
    pengarang = StringField(required=True, max_length=70)
    tahun_terbit = StringField(required=True, max_length=20)
    genre = StringField(required=True, max_length=70)
    

