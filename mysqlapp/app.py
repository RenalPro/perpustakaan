from models.customers import database
from flask import Flask

app = Flask(__name__)


@app.route("/")
def main():
    return "Welcome!"

  

if __name__ == "__main__":
    mysqldb = database()
    if mysqldb.db.is_connected():
        print('Connected to MySQL database')
    
    app.run(debug=True)
    
    if mysqldb.db is not None and mysqldb.db.is_connected():
        mysqldb.db.close()